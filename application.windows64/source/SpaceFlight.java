import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import processing.sound.*; 
import java.util.Arrays; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SpaceFlight extends PApplet {

//Global variable declaration
float xPos = 0;
float yPos = 360;
float numFrames = 0;
float time = 0;
int killX[] = new int[100];
int killY[] = new int[100];
int gameStatus = 0; //0 = menu, 1 = in game, 2 = lose screen
int background = 0;
int rockDiameterX = 50;
int rockDiameterY = 50;
int numRocks = 1;
float xModifier = 1;
int minRocks = 1;
int maxRocks = 5;
int turn = 1;
int tempX = 0;
int tempY = 0;
PImage spaceship;
PImage spaceshipOn;
PImage explosion;
int animation = 0; //controls which character image is displayed

SoundFile file;
SoundFile file2;

int numStars = PApplet.parseInt (random(25,50));
Star[] star = new Star[51];
float score = 0;
float finalScore = 0;

public void setup(){
  
  textSize(32);
  fill(0, 102, 153);
  spaceship = loadImage("spaceship.png");
  spaceshipOn = loadImage("spaceshipOn.png");
  explosion = loadImage("explosion.png");
  file = new SoundFile(this, "wilhelmScream.wav");
  file2 = new SoundFile(this, "explosion.wav");
  for(int i = 0;i<50;i++){
    star[i] = new Star();
  }
}

public void draw(){
  //Title Screen
  if (gameStatus == 0){
    background(0);
    for(int i = 0;i<numStars;i++){
       star[i].create();
    }
    textSize(80);
    fill(255);
    text("Space Flight",245,250);
    textSize(32);
    fill(0, 102, 153);
    text("Press any key to start",305,500);
    image(spaceshipOn,425,350);
    //Generate Stars
    if(keyPressed){
      gameStatus = 1; 
    }
    
  }
  //In Game
  else if (gameStatus == 1){
     background(background);
     //Turn 1-specific stuff
     if (turn == 1){
       killX[0] = 500;
       killY[0] = 360;
       textSize(32);
       fill(0, 102, 153);
       text("Drag mouse to avoid the grey-brown asteroids", 110, 500);
     }
     //Generate Stars
     for(int i = 0;i<numStars;i++){
       star[i].create();
     }
     //Generate Asteroids
     stroke(0);
     fill(40, 30, 20);
     for (int i = 0;i<numRocks;i++){
       ellipse(killX[i],killY[i],rockDiameterX,rockDiameterY);
     }
     //Controls which image is displayed to make it look like the ship is animated
     if((numFrames % 15) == 0){
       if(animation == 0){
         animation = 1; 
       }
       else{
         animation = 0; 
       }
     }
     if(animation == 0){
       image(spaceship,xPos,yPos); //image(PImageVariable,xposition,yposition)
     }
     else{
       image(spaceshipOn,xPos,yPos); //image(PImageVariable,xposition,yposition)
     }
     //Time played display
     numFrames++;
     time = numFrames/60;
     fill(255);
     text("Time played: " + time, 50, 100);
     //Score
     score = 2 * numFrames;
     text("Score: " + score, 600, 100);
     //Updates horizontal position based on the current x-modifier
     xPos = xPos + xModifier;
     //mouse controls vertical movement
     yPos = mouseY;
     //Screen wrapping
     if (xPos >= 960){
        xPos = 0; //Screen wrap
        background = PApplet.parseInt(random(0,155)); //change background to random greyscale color
        numStars = PApplet.parseInt (random(25,50)); //set number of stars
        //spawn new asteroids, wipe old kill positions
        numRocks = PApplet.parseInt(random(minRocks,maxRocks));
        for (int i = 0;i<numRocks;i++){
           killX[i] = PApplet.parseInt(random(100,960));
           killY[i] = PApplet.parseInt(random(0,720));
         }
        //increase speed
        xModifier = xModifier*1.25f; //25% speed increase per turn
        //Prevents number of rocks from overflowing beyond the allotted number of array spots of killX and killY arrays
        if (minRocks < 100){
          minRocks = minRocks + 2; //increases minimum number of rocks
        }
        if (maxRocks < 100){
          maxRocks = maxRocks + 2; //increases the max number of rocks
        }
       
     //}
        turn++; //turn counter + 1
     }
     //Colliding with rock causes deathw
      for(int i=0;i<numRocks;i++){
       if ((xPos <= (killX[i] + rockDiameterX*.5f)) && (xPos >= (killX[i] - rockDiameterX*1.35f)) && (yPos <= (killY[i] + rockDiameterY*.25f)) && (yPos >= (killY[i] - rockDiameterY*1.15f))) {
         finalScore = score;
         image(explosion,xPos,yPos);
         file2.play(); //explosion
         file.play(); //Play Wilhelm Scream
         gameStatus = 2; //Set endgame screen
       }
      }
   }
   //Loss Screen
   else if (gameStatus == 2){
    background(0);
    for(int i = 0;i<numStars;i++){
       star[i].create();
    }
    textSize(32);
    fill(255);
    text("You have died",350,260);
    textSize(32);
    fill(0, 102, 153);
    text("Final Score: " + finalScore,335,350);
    image(explosion,570,220);
    fill(255, 50, 25);
    text("Press any key to close game.",260,550);
    if(keyPressed){
      exit();
    }
   }
}
class Star {
  
  int xPosition;
  int yPosition;
  int diameter;
  int red;
  int green;
  int blue;
  
  Star(){
    xPosition = PApplet.parseInt(random(0,960));
    yPosition = PApplet.parseInt(random(0,720));
    diameter = PApplet.parseInt(random(10,20));
    red = PApplet.parseInt(random(0,255));
    green = PApplet.parseInt(random(0,255));
    blue = PApplet.parseInt(random(0,255));
  }

  public void create(){
    fill(red, green, blue);
    ellipse(xPosition,yPosition,diameter,diameter);
  }
}
  public void settings() {  size(960,720); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "SpaceFlight" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}

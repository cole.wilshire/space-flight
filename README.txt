Description:

Space Flight is an "endless runner" game, where the only goal is to not hit asteroids for as long as possible.
Every time the spaceship touches the border of the screen, the space ship gets faster and more asteroids spawn.

Controls:

Moving the mouse up and down moves the ship vertically. Horizontal movement is automatic.
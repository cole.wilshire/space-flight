class Star {
  
  int xPosition;
  int yPosition;
  int diameter;
  int red;
  int green;
  int blue;
  
  Star(){
    xPosition = int(random(0,960));
    yPosition = int(random(0,720));
    diameter = int(random(10,20));
    red = int(random(0,255));
    green = int(random(0,255));
    blue = int(random(0,255));
  }

  void create(){
    fill(red, green, blue);
    ellipse(xPosition,yPosition,diameter,diameter);
  }
}
